# Public Presentations
This is a public repository for various presentation slides for Tim Thomas since 2017. Please feel free to [email](mailto:t77@uw.edu?Subject=Contact%20through%20gitlab%20talks%20repo) me if you have any questions or clarifications.

The [Presentation_PDFs](https://gitlab.com/timathomas/talks/tree/master/Presentation_PDFs) folder holds the most readable version of the slides across various platforms sorted from earliest to latest.